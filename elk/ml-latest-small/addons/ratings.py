import csv
from collections import deque
import elasticsearch
from elasticsearch import helpers

def readMovies():
    csvfile = open('ml-latest-small/movies.csv', 'r')
    reader = csv.DictReader( csvfile )
    titleLookup = {}
    for movie in reader:
        titleLookup[movie['movieId']] = movie['title']
    return titleLookup

def readRatings():
    csvfile = open('ml-latest-small/ratings.csv', 'r')
    titleLookup = readMovies()
    reader = csv.DictReader( csvfile )
    for line in reader:
        rating = {}
        rating['user_id'] = int(line['userId'])
        rating['movie_id'] = int(line['movieId'])
        rating['title'] = titleLookup[line['movieId']]
        rating['rating'] = float(line['rating'])
        rating['timestamp'] = int(line['timestamp'])
        yield rating

def generateFile():
    id = 0
    for rating in readRatings():
        rating["id"] = id
        print('{ "create" : { "_index": "ratings", "_type": "_doc", "_id" : "' + str(id) + '" } }')
        rating['title']=rating['title'].replace("'", "@@@")
        print(str(rating).replace("'", "\"").replace("@@@", "'"))
        id += 1

def putToElastic():
    es = elasticsearch.Elasticsearch()
    #es.indices.delete(index="ratings",ignore=404)
    deque(helpers.parallel_bulk(es, readRatings(), index="ratings", doc_type="_doc"), maxlen=0)
    es.indices.refresh()

#choose one:

generateFile()
# putToElastic()
